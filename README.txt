INSTRUCCIONES PARA JUGADOR 1:
  - Pulsa la tecla w para que tu raqueta ascienda.
  - Pulsa la tecla s para que tu raqueta descienda.

INSTRUCCIONES PARA JUGADOR 2:
  - Pulsa la tecla o para que tu raqueta ascienda.
  - Pulsa la tecla l para que tu raqueta descienda.

TRANSCURSO DEL JUEGO:
  - La pelota se irá moviendo por toda la pantalla, a la vez que va disminuyendo su tamaño poco a poco.
  - Si choca contra la pared de arriba o la de abajo, rebotará sin ningún otro cambio.
  - Si choca contra una de las raquetas, rebotará sin ningún otro cambio.
  - Si choca contra la pared izquierda o la derecha (sin chocar por tanto con la raqueta) el jugador del lado 
  contrario a esa pared sumará 1 punto a su marcador.
