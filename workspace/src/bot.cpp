#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h> //Definition of AT_* constants
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

int main() {
	int fd;
	DatosMemCompartida *pdatosCompartidos;
	fd = open("/tmp/datosCompartidos", O_RDWR);
	if (fd < 0) {
    	perror("Error al abrir el fichero.");
        exit(1);
    }   
    pdatosCompartidos = static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if (pdatosCompartidos == MAP_FAILED) {
		perror("Error al proyectar el fichero en memoria.");
		close(fd);
		return(1);
	}
	close(fd);
	while(pdatosCompartidos != NULL) {
		if(pdatosCompartidos->esfera.centro.y < pdatosCompartidos->raqueta1.getCentro().y)
			pdatosCompartidos->accion = -1;
		else if(pdatosCompartidos->esfera.centro.y == pdatosCompartidos->raqueta1.getCentro().y)
			pdatosCompartidos->accion = 0;
		else if(pdatosCompartidos->esfera.centro.y > pdatosCompartidos->raqueta1.getCentro().y)
			pdatosCompartidos->accion = 1;
		usleep(25000);
	}
	//munmap(pdatosCompartidos, sizeof(DatosMemCompartida));
	if (unlink("/tmp/datosCompartidos") < 0) {
		perror ("Error al eliminar /tmp/datosCompartidos");
		return(1);
	}
	return 1;    
}
