#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> //Definition of AT_* constants
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include"Puntuaciones.h"

int main() {
	int fd, n_read;
	Puntuaciones puntos;
	//Crear la tubería
	if (mkfifo("/tmp/FIFO_LOGGER", 0600) < 0) {
		perror("Error al crearse la tubería.");
		return 1;
	}
	//Abrir la tubería en modo lectura
	if ((fd = open("/tmp/FIFO_LOGGER", O_RDONLY)) < 0) {
		perror("Error al abrir la tubería.");
		return 1;
	}
	while (n_read = read(fd, &puntos, sizeof(puntos)) == sizeof(puntos)) {
		if(puntos.lastWinner == 1)      //Si el último ganador es el jugador 1
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador1);
		else if(puntos.lastWinner == 2) //Si el último ganador es el jugador 2
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador2);
	}
	if(n_read < 0) {
		perror("Error al leer el contenido de la tubería.");
		close(fd);
		return 1;
	}
	close(fd);
	if (unlink("/tmp/FIFO_LOGGER") < 0) {
		perror ("Error al eliminar /tmp/FIFO_LOGGER");
		exit(1);
	}
	return(0);
}
